﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
       public UserReg userReg;
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {
            userReg = new UserReg();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            userReg.Name = textBox1.Text;
            userReg.Surname = textBox2.Text;
            userReg.Age = textBox3.Text;
            
            Hide();
           
        }
    }
    public class UserReg
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Age { get; set; }
    }
}
