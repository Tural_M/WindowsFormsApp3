﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView1.SelectedItems)
            {

            listView1.Items.Remove(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.ShowDialog();
            string[] str = { form2.userReg.Name, form2.userReg.Surname, form2.userReg.Age };
            ListViewItem item = new ListViewItem(str);
            listView1.Items.Add(item);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
          
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            
            form2.ShowDialog();
            listView1.SelectedItems[0].SubItems[0].Text = form2.userReg.Name;
            listView1.SelectedItems[0].SubItems[1].Text = form2.userReg.Surname;
            listView1.SelectedItems[0].SubItems[2].Text = form2.userReg.Age;
        }
    }
}
